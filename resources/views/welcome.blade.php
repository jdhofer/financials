<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato', sans-serif;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
            font-size: 45px;
        }

        .content {
            text-align: center;
            display: inline-block;
        ;
        }

        .title {
            font-size: 96px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Financial Services</div>
        <div class="container">
            <a href="{{ action('CustomerController@index') }}">Customers</a> |
            <a href="{{ action('StockController@index') }}">Stocks</a> |
            <a href="{{ action('InvestmentController@index') }}">Investments</a>
        </div>
    </div>
</div>
</body>
</html>
