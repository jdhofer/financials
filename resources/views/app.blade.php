<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Financial</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
            position: fixed;
            top: 0;
            width: 100%;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #111;
        }
    </style>
</head>
<body>
<ul>
    <li><a href="{{ action('CustomerController@index') }}">Customers</a></li>
    <li><a href="{{ action('StockController@index') }}">Stocks</a></li>
    <li><a href="{{ action('InvestmentController@index') }}">Investments</a></li>
</ul>
<hr>
<div class="container">
    @yield('content')
</div>
</body>
</html>
